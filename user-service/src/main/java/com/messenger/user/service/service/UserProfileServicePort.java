package com.messenger.user.service.service;

import com.messenger.user.service.dto.UserProfileCreateRequest;
import com.messenger.user.service.dto.UserProfileDto;

import java.util.List;

public interface UserProfileServicePort {

    void create(UserProfileCreateRequest request);

    UserProfileDto view(Integer userId);

    UserProfileDto update(UserProfileDto profileDto);

    void remove(Integer userId);

    UserProfileDto getByLogin(String login);

    List<UserProfileDto> getAllByLogin(List<String> login);
}
