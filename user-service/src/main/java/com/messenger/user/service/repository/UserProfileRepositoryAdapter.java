package com.messenger.user.service.repository;

import com.messenger.user.service.entity.UserProfile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class UserProfileRepositoryAdapter implements UserProfileRepositoryPort {

    private final UserProfileJpaRepository profileJpaRepository;

    public UserProfileRepositoryAdapter(UserProfileJpaRepository profileJpaRepository) {
        this.profileJpaRepository = profileJpaRepository;
    }

    @Transactional
    public UserProfile create(UserProfile userProfile) {
        return profileJpaRepository.save(userProfile);
    }

    @Override
    public UserProfile find(Long profileId) {
        return profileJpaRepository.getOne(profileId);
    }

    @Transactional
    public void remove(Long profileId) {
        profileJpaRepository.deleteById(profileId);
    }

    @Transactional
    public UserProfile update(UserProfile profile) {
        return profileJpaRepository.save(profile);
    }
}
