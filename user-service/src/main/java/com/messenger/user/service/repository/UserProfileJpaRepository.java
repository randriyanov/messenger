package com.messenger.user.service.repository;

import com.messenger.user.service.entity.UserProfile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserProfileJpaRepository extends JpaRepository<UserProfile, Long> {
}
