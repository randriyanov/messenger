package com.messenger.user.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserProfileCreateRequest {

    @NotBlank()
    @Size(min = 2, max = 40)
    private String firstName;

    @NotBlank()
    @Size(min = 2, max = 40)
    private String lastName;

    @NotBlank()
    @Size(min = 2, max = 40)
    private String fatherName;

    @Email()
    @NotBlank()
    @Size(max = 40)
    private String email;

    @NotBlank()
    @Size(min = 6, max = 100)
    private String password;

}
