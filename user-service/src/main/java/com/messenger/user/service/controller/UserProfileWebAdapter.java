package com.messenger.user.service.controller;

import com.messenger.user.service.dto.UserProfileCreateRequest;
import com.messenger.user.service.dto.UserProfileDto;
import com.messenger.user.service.service.UserProfileServicePort;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/profile/")
public class UserProfileWebAdapter implements UserProfileWebPort {

    private final UserProfileServicePort profileServicePort;

    public UserProfileWebAdapter(UserProfileServicePort profileServicePort) {
        this.profileServicePort = profileServicePort;
    }


    public void create(UserProfileCreateRequest request) {

    }


    public UserProfileDto view(Integer userId) {
        return null;
    }


    public UserProfileDto update(UserProfileDto profileDto) {
        return null;
    }


    public void remove(Integer userId) {

    }

    @Override
    public UserProfileDto getByLogin(String login) {
        return null;
    }

    @Override
    public List<UserProfileDto> getAllByLogin(List<String> login) {
        return null;
    }
}
