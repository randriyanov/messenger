package com.messenger.user.service.controller;

import com.messenger.user.service.dto.UserProfileCreateRequest;
import com.messenger.user.service.dto.UserProfileDto;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface UserProfileWebPort {

    @PostMapping
    void create(@RequestBody UserProfileCreateRequest request);

    @GetMapping("{id}")
    UserProfileDto view(@PathVariable Integer userId);

    @PatchMapping("{id}")
    UserProfileDto update(@PathVariable UserProfileDto profileDto);

    @DeleteMapping("{id}")
    void remove(@PathVariable Integer userId);

    @GetMapping("login/{login}")
    UserProfileDto getByLogin(@PathVariable String login);

    @GetMapping("login")
    List<UserProfileDto> getAllByLogin(@RequestParam List<String> logins);
}
