package com.messenger.user.service.repository;

import com.messenger.user.service.entity.UserProfile;

public interface UserProfileRepositoryPort {

    UserProfile create(UserProfile userProfile);

    UserProfile find(Long profileId);

    void remove(Long profileId);

    UserProfile update(UserProfile profile);
}
