### Service provides configurations for the other services
[How to Run]

[Programmatic Way]
Navigate to MessengerConfigurationApplication and run as java application
or in Intellij-> Gradle Panel-> :messenger-configuration -> application -> bootRun

[Using Docker]

1. run gradle clean :messenger-configuration:docker or ./gradlew clean :messenger-configuration:docker
2. docker run --rm -e 'SPRING_PROFILES_ACTIVE=native' -p 8888:8888 messenger/messenger-configuration

[Using Docker Compose]

1. Run ./gradlew clean build