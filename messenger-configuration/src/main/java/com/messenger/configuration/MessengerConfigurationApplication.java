package com.messenger.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class MessengerConfigurationApplication {

    public static void main(String[] args) {
        SpringApplication.run(MessengerConfigurationApplication.class, args);
    }
}
