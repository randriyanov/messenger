buildscript {
    apply from: "${rootProject.projectDir}/buildSrc/repositories.gradle"
    repositories extRepositories
    apply from: "${rootProject.projectDir}/buildSrc/buildscript-dependencies.gradle"
    dependencies extDependencies
}

group 'messenger'
version '1.0-SNAPSHOT'

repositories {
    mavenCentral()
}

configure(allprojects) { project ->
    apply plugin: 'io.spring.dependency-management'
    apply plugin: 'com.palantir.docker'
    apply plugin: 'java'
    apply plugin: 'idea'
    apply from: "${rootProject.projectDir}/buildSrc/dependencies.gradle"

    if(project.name != 'messenger') {
        bootJar {
            enabled = false
        }
    }

    apply plugin: 'org.springframework.boot'
    docker {
        name "messenger/${bootJar.baseName}"
        tags project.version
        println "==> Docker task for ${name}"
        files bootJar.archivePath
        buildArgs(['JAR_FILE': "${bootJar.archiveName}"])
        dependsOn build
    }

    sourceCompatibility = 11
    repositories extRepositories

    test {
        useJUnitPlatform {
            includeEngines 'junit-jupiter'
        }
        testLogging {
            events 'PASSED', 'FAILED', 'SKIPPED'
        }
        failFast = false
    }

    dependencies {
        annotationProcessor boot.configurationProcessor
        annotationProcessor util.lombok
        compileOnly util.lombok

        testRuntime testing.junit5Engine
        testImplementation testing.junit5Api
        testCompile testing.junit5Params
        testCompile testing.mockitoJunitJupiter
        testCompile testing.mockitoCore
        testCompile boot.starterTest
    }

    dependencyManagement {
        imports {
            mavenBom "org.springframework.cloud:spring-cloud-dependencies:${springCloudVersion}"
            mavenBom(org.springframework.boot.gradle.plugin.SpringBootPlugin.BOM_COORDINATES)
        }
    }


}

project(':messenger-domain') {
    
}

project(':messenger-configuration') {

    bootJar {
        archivesBaseName = 'messenger-configuration'
        mainClassName = 'com.messenger.configuration.MessengerConfigurationApplication'
    }

    dependencies {
        implementation boot.actuator
        implementation bootCloud.configServer
        implementation bootCloud.eurekaClient
    }
    
}

project(':chat-service') {

    bootJar {
        archivesBaseName = 'chat-service'
        mainClassName = 'com.messenger.chat.service.ChatServiceApplication'
    }

    dependencies {
        implementation boot.actuator
        implementation bootCloud.configServer
        implementation bootCloud.eurekaClient
    }
}

project(':chat-subscriber') {

    bootJar {
        archivesBaseName = 'chat-subscriber'
        mainClassName = 'com.messenger.chat.service.ChatServiceApplication'
    }

    dependencies {
        implementation boot.actuator
        implementation bootCloud.configServer
        implementation bootCloud.eurekaClient
    }
}

project(':messenger-gateway') {
    bootJar {
        archivesBaseName = 'messenger-gateway'
        mainClassName = 'com.messenger.configuration.MessengerGatewayApplication'
    }

    dependencies {
        implementation boot.actuator
        implementation bootCloud.configServer
        implementation bootCloud.eurekaClient
    }
}

project(':group-service') {
    bootJar {
        archivesBaseName = 'group-service'
        mainClassName = 'com.messenger.group.service.MessengerGroupApplication'
    }

    dependencies {
        implementation boot.actuator
        implementation boot.starterJpa
        implementation bootCloud.configServer
        implementation bootCloud.eurekaClient
        implementation bootCloud.hystrixClient
        implementation bootCloud.openFeign
        implementation db.postgres
        compile project(':messenger-domain')
    }
}

project(':user-service') {
    bootJar {
        archivesBaseName = 'user-service'
        mainClassName = 'com.messenger.user.service.UserServiceApplication'
    }

    dependencies {
        implementation boot.actuator
        implementation boot.starterJpa
        implementation bootCloud.configServer
        implementation bootCloud.eurekaClient
        implementation bootCloud.hystrixClient
        implementation db.postgres
        compile project(':messenger-domain')
    }
}

project(':messenger-discovery') {
    bootJar {
        archivesBaseName = 'messenger-discovery'
        mainClassName = 'com.ra.messenger.discovery.MessengerDiscoveryApplication'
    }

    dependencies {
        implementation boot.actuator
        implementation bootCloud.configStarter
        implementation bootCloud.eurekaServer
        implementation misc.jaxbApi
        implementation misc.jaxbCore
        implementation misc.jaxbImpl
        implementation misc.activation
    }
}

docker.dependsOn build

wrapper {
    version = '5.2.1'
}
