package com.messenger.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

@SpringCloudApplication
public class MessengerGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(MessengerGatewayApplication.class, args);
    }
}
