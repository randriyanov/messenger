package com.messenger.group.service.service;

import com.messenger.user.service.dto.UserProfileDto;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

import static java.util.Collections.emptyList;

@Slf4j
public class UserProfileClientFallbackFactory implements FallbackFactory<UserProfileClient> {

    @Override
    public UserProfileClient create(Throwable cause) {
        return new UserProfileClient() {

            @Override
            public UserProfileDto getUserByLogin(String login) {
                log.warn("Failed to load profile dto by login {}", login);
                return null;
            }

            @Override
            public List<UserProfileDto> getAllByLogin(List<String> logins) {
                log.warn("Failed to get profile dto list");
                return emptyList();
            }
        };
    }

}
