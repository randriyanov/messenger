package com.messenger.group.service.controller;

import com.messenger.group.service.dto.GroupCreateRequest;
import com.messenger.group.service.dto.GroupDto;
import com.messenger.group.service.dto.GroupUpdateRequestDto;
import com.messenger.user.service.dto.UserProfileDto;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface GroupWebPort {

    @PostMapping
    void create(@RequestBody GroupCreateRequest request);

    @GetMapping("{id}")
    GroupDto view(@PathVariable Integer groupId);

    @PatchMapping("{id}")
    UserProfileDto update(@PathVariable GroupUpdateRequestDto requestDto);

    @DeleteMapping("{id}")
    void remove(@PathVariable Integer groupId);

    @GetMapping("login/{login}")
    UserProfileDto getByLogin(@PathVariable String login);

    @GetMapping("login")
    List<UserProfileDto> getAllByLogin(@RequestParam List<String> logins);
}
