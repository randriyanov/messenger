package com.messenger.group.service.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "profile_meta_data",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "profileId")
        })
@Access(AccessType.FIELD)
@Getter
@Setter
@NoArgsConstructor
public class ProfileMetaData {

        @Id
        @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "profile_meta_data_seq")
        @SequenceGenerator(name = "profile_meta_data_seq", sequenceName = "profile_meta_data_id_seq")
        private Long id;
        private Long profileId;
}
