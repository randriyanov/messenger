package com.messenger.group.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GroupDto {

    private Long groupId;
    private String groupName;
    private List<String> logins;
    private List<Long> userIds;
}
