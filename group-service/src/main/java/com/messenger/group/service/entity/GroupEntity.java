package com.messenger.group.service.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "group")
@Access(AccessType.FIELD)
@Getter
@Setter
@NoArgsConstructor
public class GroupEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "profile_meta_data_seq")
    @SequenceGenerator(name = "profile_meta_data_seq", sequenceName = "profile_meta_data_id_seq")
    private Long id;

    @ManyToMany
    @JoinTable(name = "profile_meta_date", joinColumns = @JoinColumn(name = "group_id"),
            inverseJoinColumns = @JoinColumn(name = "profile_id"))
    private Set<ProfileMetaData> metaData = new HashSet<>();

    private String groupName;
}
