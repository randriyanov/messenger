package com.messenger.group.service.service;

import com.messenger.user.service.dto.UserProfileDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@FeignClient(name = "user-service", fallbackFactory = UserProfileClientFallbackFactory.class)
public interface UserProfileClient {

    @GetMapping(value = "/profile/login/{login}", consumes = APPLICATION_JSON_VALUE)
    UserProfileDto getUserByLogin(@PathVariable String login);

    @GetMapping(value = "/profile/login", consumes = APPLICATION_JSON_VALUE)
    List<UserProfileDto> getAllByLogin(@RequestParam List<String> logins);
}