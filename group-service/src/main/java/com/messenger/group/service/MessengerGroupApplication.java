package com.messenger.group.service;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

@SpringCloudApplication
public class MessengerGroupApplication {
    public static void main(String[] args) {
        SpringApplication.run(MessengerGroupApplication.class, args);
    }
}
